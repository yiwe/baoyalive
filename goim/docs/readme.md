### go docs
一、下载go-swagger
```
go-swagger 官方下载
根据不同个的操作系统选择对应的
https://github.com/go-swagger/go-swagger/releases
```
二、添加环境变量
```
2.1 window
swagger_windows_amd64.exe
将swagger_windows_amd64.exe  重命名 成 swagger.exe
然后将该软件放到$GOROOT/bin 中
```

2.2 linux
```
swagger_linux_amd64
重命名成 swagger
然后 将 软件放入$GOROOT/bin 目录下
mv swagger $GOROOT/bin/
```
三、关于swagger-UI 的页面

如果你想要 petstore.swagger.io 的那种页面 ，你可以自己下载 https://github.com/swagger-api/swagger-ui ,

下载好后进入dist 页面执行运行index.html 即可

四、go-swagger 规范
```
// swagger:operation PUT /api/v1/addr/update/{id} addr del
// ---
// summary: 修改用户地址
// description: 修改指定用户的addr
// parameters:
// - name: token
//   in: header
//   description: token
//   type: string
//   required: true
// - name: id
//   in: path
//   description: 地址id  
//   type: string
//   required: true
// - name: addr
//   in: body
//   description: addr
//   type: string
//   required: true
// responses:
//   200: repoResp
//   400: badReq
```

你可以将上面的规范放在go源码中每个controller 函数 前面
解释
```
// swagger:operaion [请求方式(可以是GET\PUT\DELETE\POST\PATCH)] [url:请求地址] [tag] [operation id]  （同一标签的属于同一类，）
// --- 这个部分下面是YAML格式的swagger规范.确保您的缩进是一致的和正确的
// summary: 标题
// description: 描述
// parametres:   下面是参数了
// - name: 参数名
    in: [header|body|query|path] 参数的位置 header和body 之http的header和body 位置。 query 是http://url?query  path 就是url 里面的替换信息
    description: 描述
    type: 类型
    required: 是否必须
// responses: 响应
// 200：
// 404：
```

五 、go-swagger 使用
进入自己项目的根目录
    swagger 会自己寻找main 包的
执行命令
```
swagger generate spec -o ./swagger.json    // 根据swagger规范 创建 swagger.json 规范文档
swagger serve -F=swagger swagger.json     // 启动一个http 服务同时将json文档放入http://petstore.swagger.io 执行
```
